const ShareIcon = () => {
  return (
    <svg width="17" height="17" viewBox="0 0 17 17" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path
        d="M9.16675 8.09264L14.6334 2.62598"
        stroke="white"
        stroke-width="1.5"
        stroke-linecap="round"
        stroke-linejoin="round"
      />
      <path
        d="M15.1666 5.29277V2.09277H11.9666"
        stroke="white"
        stroke-width="1.5"
        stroke-linecap="round"
        stroke-linejoin="round"
      />
      <path
        d="M7.83325 2.09277H6.49992C3.16659 2.09277 1.83325 3.42611 1.83325 6.75944V10.7594C1.83325 14.0928 3.16659 15.4261 6.49992 15.4261H10.4999C13.8333 15.4261 15.1666 14.0928 15.1666 10.7594V9.42611"
        stroke="white"
        stroke-width="1.5"
        stroke-linecap="round"
        stroke-linejoin="round"
      />
    </svg>
  );
};

export default ShareIcon;
